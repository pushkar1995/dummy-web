import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';

import Header from './Components/Header';
import Home from './Components/Home';
import Vision from './Components/Vision';
import Upcoming from './Components/Upcoming';
import Team from './Components/Team';
import Footer from './Components/Footer';
import { HOME_SECTION, TEAM_SECTION, VISION_SECTION, UPCOMING_SECTION } from './Components/Constants';

function App() {
  return (
    <>
      <Header />
      <Home {...HOME_SECTION} />
      <Vision {...VISION_SECTION} />
      <Upcoming {...UPCOMING_SECTION} />
      <Team {...TEAM_SECTION} />
      <Footer />
    </>
  );
}

export default App;
