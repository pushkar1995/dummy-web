import React from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';

const Header = () => {
  return (
    <>
      <Container>
        <Navbar bg={'light'} expand={'md'}>
          <Navbar.Brand href={'#home'}>Dummy</Navbar.Brand>
          <Navbar.Toggle aria-controls={'basic-navbar-nav'} />
          <Navbar.Collapse id={'basic-navbar-nav'}>
            <Nav className={'ml-auto'}>
              <Nav.Link href={'#home'}>Home</Nav.Link>
              <Nav.Link href={'#vision'}>Vision</Nav.Link>
              <Nav.Link href={'#upcoming'}>Upcoming</Nav.Link>
              <Nav.Link href={'#team'}>Team</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </Container>
    </>
  );
};

export default Header;
