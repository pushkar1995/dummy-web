import React from 'react';
import { Container, Row, Col, Nav } from 'react-bootstrap';

const Footer = () => {
  return (
    <>
      <Container id={'footer'}>
        <Row>
          <Col xs={12} md={4}>
            <h2> Dummy </h2>
            <p>Lorem Ipsum is simply dummy text of the printing and type.</p>
          </Col>
          <Col xs={12} md={4}>
            <h3>Useful link:</h3>
            <Nav className={'justify-content-center flex-column'}>
              <Nav.Item>
                <Nav.Link href={'/home'}>Home</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link href={'/vision'}>Vision</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link href={'/upcoming'}>Upcoming</Nav.Link>
              </Nav.Item>
              <Nav.Item>
                <Nav.Link href={'/team'}>Team</Nav.Link>
              </Nav.Item>
            </Nav>
          </Col>
          <Col xs={12} md={4}>
            <h3>Contact:</h3>
          </Col>
        </Row>
        <hr className={'featurette-divider'}></hr>
        <Nav
          className={'justify-content-end'}
          activeKey={'/home'}
          onSelect={selectedKey => alert(`selected ${selectedKey}`)}
        >
          <Nav.Item className={'mr-auto'}>
            <Nav.Link href={'/home'}>© 2020</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey={'/help'}>Help</Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey={'Terms &amp; Condition'}>
              Terms &amp; Condition
            </Nav.Link>
          </Nav.Item>
          <Nav.Item>
            <Nav.Link eventKey={'Privacy'}>Privacy</Nav.Link>
          </Nav.Item>
        </Nav>
      </Container>
    </>
  );
};

export default Footer;
