import React from 'react';
import { Container, Row, Col, Card } from 'react-bootstrap';
import CardItem from './CardItem';

interface TeamObj {
  id: number | string;
  image: string;
  name: string;
  designation: string;
  description: string;
  alt: string;
}

interface TeamProps {
  head: string;
  title: string;
  teams: TeamObj[];
}

const Team: React.FC<TeamProps> = ({ head, title, teams }) => {
  const teamList = teams.map((team: TeamObj, index: number) => (
    <CardItem key={index} {...team} />
  ));

  return (
    <>
      <Container id={'team'}>
        <Row>
          <Col>
            <h2 className={'head text-center'}> {head} </h2>
          </Col>
        </Row>
        <Row>
          <Col>
            <h4 className={'title text-center'}> {title} </h4>
          </Col>
        </Row>
        <Card>{teamList}</Card>
      </Container>
    </>
  );
};

export default Team;
