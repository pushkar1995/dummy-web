import React, { useState } from 'react';
import { Card, Button, Modal } from 'react-bootstrap';

interface CardItemProps {
  image: string;
  name: string;
  designation: string;
  description: string;
}

const CardItem: React.FC<CardItemProps> = ({
  image,
  name,
  designation,
  description
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const handleClick = (isOpen: boolean) => setIsOpen(isOpen);

  return (
    <>
      <Card border={'light'}>
        <Card.Img src={image} />
        <Card.Body>
          <Card.Title> {name} </Card.Title>
          <Card.Subtitle className={'title'}> {designation} </Card.Subtitle>
          {description.length > 100 ? (
            <>
              <Card.Text> {description.slice(0, 100)}..... </Card.Text>
              <Button variant={'info'} onClick={() => handleClick(true)}>
                Read More
              </Button>
            </>
          ) : (
            <Card.Text> {description} </Card.Text>
          )}
        </Card.Body>
      </Card>
      <Modal show={isOpen} onHide={() => handleClick(false)}>
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          <Card>
            <Card.Img src={image} />
            <Card.Body>
              <Card.Title> {name} </Card.Title>
              <Card.Subtitle className={'title'}> {designation} </Card.Subtitle>
              <Card.Text> {description} </Card.Text>
            </Card.Body>
          </Card>
        </Modal.Body>
      </Modal>
    </>
  );
};

export default CardItem;
