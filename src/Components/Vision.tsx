import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';

interface VisionProps {
  head: string;
  title: string;
  content: string;
  image: string;
}

const Vision: React.FC<VisionProps> = ({ head, title, content, image }) => {
  return (
    <>
      <Container id={'vision'}>
        <Row>
          <Col xs={12} md={6}>
            <Image rounded fluid src={image} />
          </Col>
          <Col xs={12} md={6}>
            <h2 className={'head'}> {head} </h2>
            <h4 className={'title'}> {title} </h4>
            <p> {content} </p>
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Vision;
