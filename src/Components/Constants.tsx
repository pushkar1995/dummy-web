export const HOME_SECTION = {
  head: 'Home Section',
  title: 'Here goes the title of our home section',
  content: 'Here is the description',
  image: 'https://html.themexriver.com/Saasio/assets/img/d-agency/banner/ns1.png'
};

export const VISION_SECTION = {
  head: 'Our Vision',
  title: 'Here goes our vision title',
  content: 'Here goes the description',
  image: 'https://html.themexriver.com/Saasio/assets/img/d-agency/service/s1.png'
};

export const UPCOMING_SECTION = {
  head: 'Our Upcomings',
  title: 'Here goes our upcoming title',
  content: 'Here goes the description',
  image: 'https://html.themexriver.com/Saasio/assets/img/d-agency/about/ab1.png'
};

export const TEAMS = [
  {
    id: 1,
    image:
      'https://html.themexriver.com/Saasio/assets/img/d-agency/team/tm1.png',
    name: 'Name',
    designation: 'Designation',
    description:
      'paraphraser contains an abundance of rarely used words/phrases and can paraphrase sentences in a variety of ways that are chosen randomly. Aside from this web based software being used as a paraphrasing tool or a text spinner',
    alt: 'designation image'
  },
  {
    id: 2,
    image:
      'https://html.themexriver.com/Saasio/assets/img/d-agency/team/tm2.png',
    name: 'Name',
    designation: 'Designation',
    description:
      ' software being used as a paraphrasing tool or a text spinner',
    alt: 'designation image'
  },
  {
    id: 3,
    image:
      'https://html.themexriver.com/Saasio/assets/img/d-agency/team/tm3.png',
    name: 'Name',
    designation: 'Designation',
    description:
      'paraphraser contains an abundance of rarely used words/phrases and can paraphrase sentences in a variety of ways that are chosen randomly. Aside from this web based software being used as a paraphrasing tool or a text spinner',
    alt: 'designation image'
  }
];

export const TEAM_SECTION = {
  head: 'Our Team',
  title: 'here goes the title of the team',
  teams: TEAMS
};
