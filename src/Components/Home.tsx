import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';

interface HomeProps {
  head: string;
  title: string;
  content: string;
  image: string;
}

const Home: React.FC<HomeProps> = ({ head, title, content, image }) => {
  return (
    <>
      <Container id={'head'}>
        <Row>
          <Col>
            <h2 className={'head'}> {head} </h2>
            <h4 className={'title'}> {title} </h4>
            <p> {content} </p>
          </Col>
          <Col xs={6}>
            <Image fluid src={image} />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Home;
