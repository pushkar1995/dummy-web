import React from 'react';
import { Container, Row, Col, Image } from 'react-bootstrap';

interface UpcomingProps {
  head: string;
  title: string;
  content: string;
  image: string;
}

const Upcoming: React.FC<UpcomingProps> = ({ head, title, content, image }) => {
  return (
    <>
      <Container id={'upcoming'}>
        <Row>
          <Col xs={12} md={6}>
            <h2 className={'head'}> {head} </h2>
            <h4 className={'title'}> {title} </h4>
            <p> {content} </p>
          </Col>
          <Col xs={12} md={6}>
            <Image rounded fluid src={image} />
          </Col>
        </Row>
      </Container>
    </>
  );
};

export default Upcoming;
